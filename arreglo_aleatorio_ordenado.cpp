#include <iostream>
#include <time.h>
#include <stdlib.h>
using namespace std;

//protitipo funciones
int  *arreglo_aleatorio_ordenado(int);

int main(){
	int tam;
	cout<<"digite el tamanio del arreglo: ";
	cin>>tam;
	
	int *arr = arreglo_aleatorio_ordenado(tam);
}

int *arreglo_aleatorio_ordenado(int tam){
	int *arr = NULL;
	if(tam%2 == 0) {
		arr = new int [7];
		tam = 7;
	}
	else arr = new int [tam];
	
	srand((unsigned int) time(NULL));
	int i=0, act = 0, ant = 0;
	while(i<tam){
		arr[i] = rand()% 101; //genera un numero aleatorio de 0 a 100
		for(act = i; act>=0; act--){
			if(act==0) ant = 0;
			else ant = act-1;	
			if(arr[ant]>arr[act]){
				int aux = arr[act];
				arr[act] = arr[ant];
				arr[ant] = aux;
			}	
		} 
		i++;
	}
	for(i=0; i<tam; i++) cout<<arr[i]<<" -> ";
	cout<<endl;
	return arr;
}
